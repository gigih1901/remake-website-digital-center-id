import Image from 'next/image'
import TestimonialImage from '@/public/images/testimonial.jpg'

export default function DigitalMarketing() {
  return (
    <section className="bg-gradient-to-b from-gray-100 to-white">
<div class="bg-white py-24 sm:py-32">
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <div class="mx-auto max-w-2xl lg:text-center">
      <h1 class="text-3xl mt-2 font-bold leading-7 text-indigo-600 sm:text-7xl">Digital Marketing</h1>
      <p class="mt-6 text-lg leading-8 text-gray-600">We’re designing digital marketing that enrich human lives and it helps to grow your business globally trends.</p>
    </div>
    <Image src="/images/dm.jpeg" width={700} height={700} alt="Hero image" />
</div>
</div>
  
          {/* Section background (needs .relative class on parent and next sibling elements) */}    
          <h1 class="text-3xl mt-2 font-bold leading-7 text-indigo-600 sm:text-5xl text-center">Approach-Creativity-Experienced</h1>

          <div className="relative max-w-6xl mx-auto px-4 sm:px-6">
            <div className="py-12 md:py-20">
              {/* Section header */}

              {/* Items */}
              <div className="max-w-sm mx-auto grid gap-6 md:grid-cols-2 lg:grid-cols-3 items-start md:max-w-2xl lg:max-w-none">
    
                {/* 1st item */}
                <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
                  <h4 className="text-xl font-bold leading-snug tracking-tight mb-1">UI/UX SERVICES</h4>
                </div>
    
                {/* 2nd item */}
                <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
                  <h4 className="text-xl font-bold leading-snug tracking-tight mb-1">PAID
ADVERTISEMENT</h4>
                </div>
    
                {/* 3rd item */}
                <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
                  <h4 className="text-xl font-bold leading-snug tracking-tight mb-1">Social Media Management</h4>
                </div>
                {/* 4th item */}
                <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
                  <h4 className="text-xl font-bold leading-snug tracking-tight mb-1">Content Marketing Service</h4>
                </div>
    
                {/* 5th item */}
                <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
                  <h4 className="text-xl font-bold leading-snug tracking-tight mb-1">Web And Design Development</h4>
                </div>
    
                {/* 6th item */}
                <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
                  <h4 className="text-xl font-bold leading-snug tracking-tight mb-1">Paid Advertisement</h4>
                </div>
                
              </div>
    
            </div>
          </div>
    </section>
  )
}