export const metadata = {
  title: 'Home - Digital Center Indonesia',
  description: 'Digital Center Indonesia',
}

import Hero from '@/components/hero'
import Features from '@/components/features'
import FeaturesBlocks from '@/components/features-blocks'
import Testimonials from '@/components/testimonials'
import Newsletter from '@/components/newsletter'
import OurFeatures from '@/components/our-features'

export default function Home() {
  return (
    <>
      <Hero />
      <FeaturesBlocks />
      <Features />
      <br></br>
      <Newsletter />
      <OurFeatures/>
      <Testimonials />
    </>
  )
}
