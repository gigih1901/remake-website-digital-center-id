import Image from "next/image";

export default function Newsletter() {
  return (
    <section>
      <div>
        <div className="pb md:pb-20">

          {/* CTA box */}
          <div className="relative bg-gray-900 rounded py-10 px-8 md:py-16 md:px-12 shadow-2xl overflow-hidden" data-aos="zoom-y-out">

            {/* Background illustration */}
            <div className="absolute right-0 bottom-0 pointer-events-none hidden lg:block" aria-hidden="true">
            <Image src="/images/collaboration.jpeg" width={400} height={400} alt="Hero image" />
            </div>

            <div className="relative flex flex-col lg:flex-row justify-between items-center">

              {/* CTA content */}
              <div className="text-center lg:text-left lg:max-w-xl">
                <h3 className="h3 text-white mb-2">We execute projects utilizing our specialized and strategic approach</h3>
                {/* CTA form */}
                <form className="w-full lg:w-auto">
                  {/* Success message */}
                  {/* <p className="text-sm text-gray-400 mt-3">Thanks for subscribing!</p> */}
                  <p className="text-sm text-gray-400 mt-3">We are perpetually innovating for the future, meticulously crafting the ensuing generation of products, brands, and design attributes from a multifaceted and hybrid viewpoint. This approach ensures that our creations are not only current but also forward-thinking. positioning them at the forefront of industry advancements and technological progress..</p>
                </form>
              </div>

            </div>

          </div>

        </div>
      </div>
    </section>
  )
}