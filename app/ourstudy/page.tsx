import Image from 'next/image'
import TestimonialImage from '@/public/images/testimonial.jpg'

export default function OurStudy() {
  return (
    <section className="relative">
            <br></br><br></br><br></br>
      {/* Illustration behind content */}
      <div className="absolute left-1/2 transform -translate-x-1/2 bottom-0 pointer-events-none -mb-32" aria-hidden="true">
        <svg width="1760" height="518" viewBox="0 0 1760 518" xmlns="http://www.w3.org/2000/svg">
          <defs>
            <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="illustration-02">
              <stop stopColor="#FFF" offset="0%" />
              <stop stopColor="#EAEAEA" offset="77.402%" />
              <stop stopColor="#DFDFDF" offset="100%" />
            </linearGradient>
          </defs>
          <g transform="translate(0 -3)" fill="url(#illustration-02)" fillRule="evenodd">
            <circle cx="1630" cy="128" r="128" />
            <circle cx="178" cy="481" r="40" />
          </g>
        </svg>
      </div>

      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        <div className="py-12 md:py-20">

          {/* Section header */}
          <div className="max-w-3xl mx-auto text-center pb-12 md:pb-16">
            <h2 className="h2 mb-4">DIGITAL­ SOLUTION­ TO­ ­ ACCELERATE­ YOUR­ BUSINESS</h2>
            <p className="text-xl text-gray-600" data-aos="zoom-y-out">WE WORKED WITH GLOBAL LARGEST BRANDS.</p>
          </div>

          {/* Items */}
          <div className="max-w-sm mx-auto grid gap-6 md:grid-cols-2 lg:grid-cols-3 items-start md:max-w-2xl lg:max-w-none">
  
  {/* 1st item */}
  <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
  <figure className="px-10 pt-10">
    <img src="https://digitalcenter.id/wp-content/uploads/2022/12/Mayback-Finance_Top-5-Google.jpeg" alt="Shoes" className="rounded-xl" />
  </figure>
  <br></br>
    <h4 className="text-xl font-bold leading-snug tracking-tight mb-1 text-center">MAYBANK FINANCE APPEARS ON TOP 5 GOOGLE</h4>
  </div>

  {/* 2nd item */}
  <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
  <figure className="px-10 pt-10">
    <img src="https://digitalcenter.id/wp-content/uploads/2023/10/BCA-Realation-Officer-1024x537-1.jpeg" alt="Shoes" className="rounded-xl" />
  </figure>
  <br></br>
    <h4 className="text-xl font-bold leading-snug tracking-tight mb-1 text-center">BCA Realation Officer</h4>
  </div>

  {/* 3rd item */}
  <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
  <figure className="px-10 pt-10">
    <img src="https://digitalcenter.id/wp-content/uploads/2023/10/Ladara-indonesia-1280x720-1.jpg" alt="Shoes" className="rounded-xl" />
  </figure>
  <br></br>
    <h4 className="text-xl font-bold leading-snug tracking-tight mb-1 text-center">Ladara The Feature-Rich Marketplace for UMKM</h4>
  </div>
  {/* 4th item */}
  <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
  <figure className="px-10 pt-10">
    <img src="https://digitalcenter.id/wp-content/uploads/2023/10/Synnex-Metrodata_11zon.jpg" alt="Shoes" className="rounded-xl" />
  </figure>
  <br></br>
    <h4 className="text-xl font-bold leading-snug tracking-tight mb-1 text-center"> Synnex Metrodata Build Your Gaming Empire (BYGE)</h4>
  </div>
  {/* 5th item */}
  <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
  <figure className="px-10 pt-10">
    <img src="https://digitalcenter.id/wp-content/uploads/2022/12/Maybank-M2U-Apps_2-1024x579-1.jpeg" alt="Shoes" className="rounded-xl" />
  </figure>
  <br></br>
    <h4 className="text-xl font-bold leading-snug tracking-tight mb-1 text-center">Maybank M2U Mobile Apps Enhancement</h4>
  </div>

  {/* 6th item */}
  <div className="relative flex flex-col items-center p-6 bg-white rounded shadow-xl">
  <figure className="px-10 pt-10">
    <img src="https://digitalcenter.id/wp-content/uploads/2024/03/WhatsApp-Image-2024-03-05-at-17.20.43.jpeg" alt="Shoes" className="rounded-xl" />
  </figure>
  <br></br>
    <h4 className="text-xl font-bold leading-snug tracking-tight mb-1 text-center">Tanam Emas : The First Innovative Gold Commerce In Indonesia</h4>
  </div>
</div>
        </div>
      </div>
    </section>
  )
}