import Link from 'next/link'
import Image from 'next/image'
export default function Logo() {
  return (
    <Link href="/" className="block" aria-label="Cruip">
    <Image src="/images/logodct.webp" alt="logo" width={200} height={200} />
    {/* <img src="/images/logodct.webp" alt="logo" /> */}
    </Link>
  )
}
